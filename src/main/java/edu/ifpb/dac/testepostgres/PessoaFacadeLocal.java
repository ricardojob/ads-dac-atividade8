/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.testepostgres;

import javax.ejb.Local;

/**
 *
 * @author Ricardo Job
 */
@Local
public interface PessoaFacadeLocal {

    public boolean salvar(Pessoa pessoa);
}
