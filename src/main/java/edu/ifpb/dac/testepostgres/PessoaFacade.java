/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.testepostgres;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Ricardo Job
 */
@Stateless
public class PessoaFacade implements PessoaFacadeLocal {

    @PersistenceContext(unitName = "edu.ifpb.dac_TestePostgres_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    public boolean salvar(Pessoa pessoa) {
        em.persist(pessoa);
        return true;
    }

}
