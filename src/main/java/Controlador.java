
import edu.ifpb.dac.testepostgres.Pessoa;
import edu.ifpb.dac.testepostgres.PessoaFacadeLocal;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Ricardo Job
 */
@ManagedBean(name = "controlador")
@SessionScoped
public class Controlador {

    private Pessoa pessoa = new Pessoa();

    @EJB
    private PessoaFacadeLocal cadastro;

    public Controlador() {
    }

    public String salvar() {
        cadastro.salvar(pessoa);
        pessoa = new Pessoa();
        return null;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

}
